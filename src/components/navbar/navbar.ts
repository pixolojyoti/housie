import { Component } from '@angular/core';
import { Storage } from '@ionic/storage';



/**
 * Generated class for the NavbarComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'navbar',
  templateUrl: 'navbar.html'
})
export class NavbarComponent {

  user: any = {};

  constructor(public storage: Storage) {
    this.storage.get('playerinfo').then((data) => {
      console.log(data)
      if (data && (data.name || data.avtar)) {
        this.user.username = data.name;
        this.user.avtar = data.avtar;
        this.user.coins = data.coins;
        // this.navCtrl.push(ConnectionoptionsPage);
      }
    })
  }

}
