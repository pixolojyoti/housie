import { Component, Input } from '@angular/core';

/**
 * Generated class for the TicketComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'ticket',
  templateUrl: 'ticket.html'
})
export class TicketComponent {
  ticketnumbers = new Array();

  @Input()
  get ticketnumber() {
    return this.ticketnumbers;
  }

  set ticketnumber(value) {
    this.ticketnumbers = value;
  }


  constructor() {

  }








}
