import { NgModule } from '@angular/core';
import { ConnectionoptionsComponent } from './connectionoptions/connectionoptions';
import { NavbarComponent } from './navbar/navbar';
import { TicketComponent } from './ticket/ticket';
@NgModule({
	declarations: [ConnectionoptionsComponent,
    NavbarComponent,
    TicketComponent],
	imports: [],
	exports: [ConnectionoptionsComponent,
    NavbarComponent,
    TicketComponent]
})
export class ComponentsModule {}
