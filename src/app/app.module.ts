
import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { IonicStorageModule } from '@ionic/storage';
import { Zeroconf } from '@ionic-native/zeroconf';
import * as $ from "jquery";

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ConnectionoptionsPage } from '../pages/connectionoptions/connectionoptions';
import { HostingpagePage } from '../pages/hostingpage/hostingpage';
import { PlayerlistpagePage } from '../pages/playerlistpage/playerlistpage';
import { HostsearchPage } from '../pages/hostsearch/hostsearch';
import { HostfoundPage } from '../pages/hostfound/hostfound';
import { EntercodePage } from '../pages/entercode/entercode';
import { ConnectingPage } from '../pages/connecting/connecting';
import { TicketPage } from '../pages/ticketpage/ticketpage';
import { SelectTicket } from '../pages/selectticket/selectticket';
import { SelectedTicket } from '../pages/selectedticket/selectedticket';
import { ConnectionProvider } from '../providers/connection/connection';
import { NavbarComponent } from '../components/navbar/navbar';
import { TicketComponent } from '../components/ticket/ticket';

@NgModule({
	declarations: [
		MyApp,
		HomePage,
		ConnectionoptionsPage,
		HostingpagePage,
		PlayerlistpagePage,
		HostsearchPage,
		TicketPage,
		SelectTicket,
		HostfoundPage,
		EntercodePage,
		SelectedTicket,
		ConnectingPage,
		TicketPage,
		NavbarComponent,
		TicketComponent

	],
	imports: [
		BrowserModule,
		IonicModule.forRoot(MyApp, {}, {
			links: [
				{ component: HomePage, name: 'Home', segment: 'home' },
				{ component: ConnectionoptionsPage, name: 'Connection', segment: 'connection' },
				{ component: HostingpagePage, name: 'Hosting', segment: 'hosting' },
				{ component: PlayerlistpagePage, name: 'players', segment: 'players' },
				{ component: HostsearchPage, name: 'Hostsearch', segment: 'hostsearch' },
				{ component: HostfoundPage, name: 'Hostfound', segment: 'hostfound' },
				{ component: EntercodePage, name: 'Entercode', segment: 'entercode' },
				{ component: ConnectingPage, name: 'Connecting', segment: 'connecting' },
				{ component: TicketPage, name: 'ticket', segment: 'ticket' },
				{ component: SelectTicket, name: 'selectticket', segment: 'selectticket' },
				{ component: SelectedTicket, name: 'selectedticket', segment: 'selectedticket' },
			]
		}),
		IonicStorageModule.forRoot({
			name: 'localStorage',
			driverOrder: ['indexeddb', 'sqlite', 'websql']
		})
		//
	],
	bootstrap: [IonicApp],
	entryComponents: [
		MyApp,
		HomePage,
		ConnectionoptionsPage,
		HostingpagePage,
		PlayerlistpagePage,
		HostsearchPage,
		TicketPage,
		SelectTicket,
		SelectedTicket,
		HostfoundPage,
		EntercodePage,
		ConnectingPage,
		NavbarComponent

	],


	providers: [
		StatusBar,
		SplashScreen,
		{ provide: ErrorHandler, useClass: IonicErrorHandler },
		ConnectionProvider,
		Zeroconf,
	]
})
export class AppModule { }
