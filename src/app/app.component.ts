import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { HomePage } from '../pages/home/home';
import { ConnectingPage } from '../pages/connecting/connecting';
import { HostsearchPage } from '../pages/hostsearch/hostsearch';
import { ConnectionoptionsPage } from '../pages/connectionoptions/connectionoptions';
import { HostingpagePage } from '../pages/hostingpage/hostingpage';
import { PlayerlistpagePage } from '../pages/playerlistpage/playerlistpage';
import { HostfoundPage } from '../pages/hostfound/hostfound';
import { TicketPage } from '../pages/ticketpage/ticketpage';
import { SelectTicket } from '../pages/selectticket/selectticket';
import { SelectedTicket } from '../pages/selectedticket/selectedticket';
@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage:any = HomePage;

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
    });
  }
}
