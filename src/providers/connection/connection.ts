import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Zeroconf } from '@ionic-native/zeroconf';
import { Storage } from '@ionic/storage';
declare var cordova: any;
/*
  Generated class for the ConnectionProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ConnectionProvider {
	static userdata: any = {};
	wsserver: any;
	connection: any;
	static hostlist = new Array();
	public static connectedusers = new Array();
	constructor(private zeroconf: Zeroconf, private storage: Storage) {
		console.log('Hello ConnectionProvider Provider');
		this.storage.get('playerinfo').then((data) => {
			console.log(data);
			if (data && data.name) {
				ConnectionProvider.userdata.username = data.name;
				ConnectionProvider.userdata.avtar = data.avtar;
				// this.navCtrl.push(ConnectionoptionsPage);
			}
		})

	}

	connectionjoin() {

		console.log('connection join method');
		this.zeroconf.reInit();
		// cordova.plugins.zeroconf.registerAddressFamily = 'ipv4';
		this.zeroconf.watch('_http._tcp.', 'local.').subscribe((result) => {
			// 	this.establishconnection('hey');

			console.log(result);
			// FINDING AND STORING ALL HOSTS
			var servicevalue = JSON.parse(result.service.name);
			var index = ConnectionProvider.hostlist.findIndex((host) => host.ip == servicevalue.ip);
			if (index == -1) {
				ConnectionProvider.hostlist.push({ userdata: servicevalue, result: result });
			}

		});





	}
	connectioncreation = () => {
		this.wsserver = cordova.plugins.wsserver;

		this.wsserver.stop();
		this.wsserver.start(0, {
			'onMessage': (conn, msg) => {
				ConnectionProvider.connectedusers.push({ 'name': msg.name,'avtar':msg.avtar, 'connectioninformation': conn });
				console.log(ConnectionProvider.connectedusers);
			}

		}, function onStart(addr, port) {
			cordova.plugins.wsserver.getInterfaces(function (result) {
				console.log('called');
				console.log(result);
				var hostdetail = JSON.stringify({ 'name': ConnectionProvider.userdata.username, 'ip': result['wlan0']['ipv4Addresses'][0], 'avtar': ConnectionProvider.userdata.avtar })
				cordova.plugins.zeroconf.register('_http._tcp.', 'local.', hostdetail, port, {

					// Publish the correct IP address on the TXT record
					server_ip: result['wlan0']['ipv4Addresses'][0]
				}, function (result) {
					console.log(result);
					// Here we have successfully advertised the service
				});
			});
		});


	}


	connecttohost(host) {
		var result = host.result;
		if (result.action == "resolved") {
			console.log('service added', result.service);
			console.log(result);
			var url = ['ws://', host.userdata.ip.replace(/[.]+$/g, ''), ':', result.service.port, '/'].join('');
			console.log(url);
			this.connection = new WebSocket(url);
			console.log(this.connection);
			this.connection.onopen = function () {

				// Now the client is connected
				console.log('connected');
				this.send(JSON.stringify({
					userdata: this.userdata
				}));
				return true;

			}
		
		}
		else {
			console.log('service removed', result.service);
			return false;
		}


	}

}
