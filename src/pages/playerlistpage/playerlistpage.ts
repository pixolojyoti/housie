import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { TicketPage } from '../ticketpage/ticketpage';
import { SelectTicket } from '../selectticket/selectticket';
import { ConnectionProvider } from '../../providers/connection/connection';
import { Storage } from '@ionic/storage';
import { NavbarComponent } from '../../components/navbar/navbar';
/**
 * Generated class for the PlayerlistpagePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-playerlistpage',
  templateUrl: 'playerlistpage.html',
})
export class PlayerlistpagePage {
  connectedusers: any;
  user: any = {};
  gametype;
  constructor(public navCtrl: NavController, public navParams: NavParams, public connectionprovider: ConnectionProvider, public storage: Storage) {
    this.storage.get('playerinfo').then((data) => {
      console.log(data);
      if (data && data.name) {
        this.user.name = data.name;
        this.user.avtar = data.avtar;
        console.log(this.user);

      }
    });
    // this.connectedusers = ConnectionProvider.connectedusers;


  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PlayerlistpagePage');
  }

  gotoselectticket() {

    this.navCtrl.push(SelectTicket);
  }

}
