import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PlayerlistpagePage } from './playerlistpage';

@NgModule({
  declarations: [
    PlayerlistpagePage,
  ],
  imports: [
    IonicPageModule.forChild(PlayerlistpagePage),
  ],
})
export class PlayerlistpagePageModule {}
