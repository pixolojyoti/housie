import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { PlayerlistpagePage } from '../playerlistpage/playerlistpage';
import { TicketComponent } from '../../components/ticket/ticket';
import { Storage } from '@ionic/storage';

/**
 * Generated class for the HostingpagePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: '',
  templateUrl: 'ticketpage.html',
})
export class TicketPage {
  selectedtickets = new Array();
  constructor(public navCtrl: NavController, public navParams: NavParams, public storage: Storage) {
    // this.navCtrl.push(PlayerlistpagePage);
    this.storage.get('tickets').then((data) => {
      this.selectedtickets = data;
      console.log(this.selectedtickets);
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TicketPage');
  }
  selectticket(parent, child) {
    // this.selectedticket[parent][child] = !this.selectedticket[parent][child];
    // console.log(this.selectedticket[parent][child]);
  }

}
