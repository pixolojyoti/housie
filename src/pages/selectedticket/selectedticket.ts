import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { TicketPage } from '../ticketpage/ticketpage';
import { NavbarComponent } from '../../components/navbar/navbar';
import { Storage } from '@ionic/storage';
import { TicketComponent } from '../../components/ticket/ticket';
/**
 * Generated class for the HostingpagePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: '',
  templateUrl: 'selectedticket.html',
})
export class SelectedTicket {
  selectedtickets = new Array();
  constructor(public navCtrl: NavController, public navParams: NavParams, public storage: Storage) {
    this.storage.get('tickets').then((data) => {
      this.selectedtickets = data;
      console.log(this.selectedtickets);
    });
    // this.navCtrl.push(SelectedTicket);
  }

  ionViewDidLoad() {

    console.log('ionViewDidLoad SelectedTicket');
  }

  gotoplaygame() {
    this.navCtrl.push(TicketPage);
  }

}
