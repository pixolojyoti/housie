import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HostfoundPage } from '../hostfound/hostfound';
import { Storage } from '@ionic/storage';
/**
 * Generated class for the HostsearchPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
	selector: 'page-hostsearch',
	templateUrl: 'hostsearch.html',
})
export class HostsearchPage {
	hostlist = new Array();
	username='';
	useravtar='';
	constructor(public navCtrl: NavController, public navParams: NavParams,private storage: Storage) {
		this.hostlist = [{ name: 'jyoti' }, { name: 'jyoti' }, { name: 'jyoti' }, { name: 'jyoti' }];
	}

	ionViewDidLoad() {
		
	}

	gotohostfound() {
		this.navCtrl.push(HostfoundPage)
	}



}
