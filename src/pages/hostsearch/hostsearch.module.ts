import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { HostsearchPage } from './hostsearch';

@NgModule({
  declarations: [
    HostsearchPage,
  ],
  imports: [
    IonicPageModule.forChild(HostsearchPage),
  ],
})
export class HostsearchPageModule {}
