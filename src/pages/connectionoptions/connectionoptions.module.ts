import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ConnectionoptionsPage } from './connectionoptions';

@NgModule({
  declarations: [
    ConnectionoptionsPage,
  ],
  imports: [
    IonicPageModule.forChild(ConnectionoptionsPage),
  ],
})
export class ConnectionoptionsPageModule {}
