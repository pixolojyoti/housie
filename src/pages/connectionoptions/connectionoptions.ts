import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HostingpagePage } from '../hostingpage/hostingpage';
import { HostfoundPage } from '../hostfound/hostfound';
import { PlayerlistpagePage } from '../playerlistpage/playerlistpage';
import { HomePage } from '../home/home';
import { Storage } from '@ionic/storage';
import { NavbarComponent } from '../../components/navbar/navbar';
/**
 * Generated class for the ConnectionoptionsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
	selector: 'page-connectionoptions',
	templateUrl: 'connectionoptions.html',
})
export class ConnectionoptionsPage {
	user: any = {};
	constructor(public navCtrl: NavController, public navParams: NavParams, public storage: Storage) {
		// this.navCtrl.push(HostingpagePage);
		this.storage.get('playerinfo').then((data) => {
			console.log(data)
			if (data && (data.name || data.avtar)) {
				this.user.username = data.name;
				this.user.avtar = data.avtar;
				// this.navCtrl.push(ConnectionoptionsPage);
			}
		})
	}

	ionViewDidLoad() {
		console.log('ionViewDidLoad ConnectionoptionsPage');
	}

	gotohostsearch() {
		this.navCtrl.push(HostfoundPage);
	}

	gotohostingpage() {
		this.navCtrl.push(PlayerlistpagePage);
	}

}
