import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { SelectTicket } from '../selectticket/selectticket';
import { ConnectionProvider } from '../../providers/connection/connection';
import { NavbarComponent } from '../../components/navbar/navbar';

/**
 * Generated class for the ConnectingPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-connecting',
  templateUrl: 'connecting.html',
})
export class ConnectingPage {
  connectedhost: any;
  user: any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public connectionProvider: ConnectionProvider) {
    this.connectedhost = navParams.get('host');
    this.user = ConnectionProvider.userdata;

  }

  ionViewDidLoad() {
    var connected = this.connectionProvider.connecttohost(this.connectedhost);
    console.log('ionViewDidLoad ConnectingPage');
    if (connected) {
      console.log('You are connected now !');

    } else {
      // Error in connection message
      console.log('can not connect now!');
    }
  }




  gotoselectticket() {
    this.navCtrl.push(SelectTicket);
  }

}
