import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { HostfoundPage } from './hostfound';

@NgModule({
  declarations: [
    HostfoundPage,
  ],
  imports: [
    IonicPageModule.forChild(HostfoundPage),
  ],
})
export class HostfoundPageModule {}
