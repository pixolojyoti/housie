import * as $ from "jquery";
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ConnectingPage } from '../connecting/connecting';
import { ConnectionProvider } from '../../providers/connection/connection';
import { Storage } from '@ionic/storage';
import { NavbarComponent } from '../../components/navbar/navbar';
/**
 * Generated class for the HostfoundPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
	selector: 'page-hostfound',
	templateUrl: 'hostfound.html',
})
export class HostfoundPage {
	hostlist = new Array();
	selectedhost: any;
	user: any = {};

	constructor(public navCtrl: NavController, public navParams: NavParams, private connectionProvider: ConnectionProvider, private storage: Storage) {
		this.storage.get('playerinfo').then((data) => {
			console.log(data);
			if (data && data.name) {
				this.user.name = data.name;
				this.user.avtar = data.avtar;
				console.log(this.user);
			}
		});
		console.log(this.hostlist.length);
		this.hostlist = [{ name: 'mrunali', avtar: 'avtar_2.png' }, { name: 'richard', avtar: 'avtar_2.png' }, { name: 'abhay', avtar: 'avtar_2.png' }];
		// this.hostlist = ConnectionProvider.hostlist;
		
		
	}

	ionViewDidLoad() {
		// this.connectionProvider.connectionjoin();
		for (var i = 0; i < this.hostlist.length; i++) {
			console.log(typeof i);
			var min, max;
			min = i % 2 == 0 ? 130 : -30;
			max = i % 2 == 0 ? 150 : -10;
			var top = Math.floor(Math.random() * (130 - 110 + 1)) + 110;
			var leftrandom = Math.floor(Math.random() * (max - min + 1)) + min;
			var toprandom = Math.floor(Math.random() * top) + 1;
			$('#userprofile' + i).css({ 'transform': 'translate('+leftrandom + 'px, '+toprandom+'px)' });

		}
		console.log('ionic view loaded');
		console.log('ionViewDidLoad HostfoundPage');
	}

	gotoconnecting() {
		this.navCtrl.push(ConnectingPage, {
			host: this.selectedhost
		});

	}
	hostselected(selectedhost) {
		this.selectedhost = selectedhost;

	}

}
