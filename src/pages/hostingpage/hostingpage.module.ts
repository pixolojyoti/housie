import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { HostingpagePage } from './hostingpage';

@NgModule({
  declarations: [
    HostingpagePage,
  ],
  imports: [
    IonicPageModule.forChild(HostingpagePage),
  ],
})
export class HostingpagePageModule {}
