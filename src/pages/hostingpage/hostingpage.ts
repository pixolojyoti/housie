import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { PlayerlistpagePage } from '../playerlistpage/playerlistpage';
import { ConnectionProvider } from '../../providers/connection/connection';
import { Storage } from '@ionic/storage';
import { NavbarComponent } from '../../components/navbar/navbar';

/**
 * Generated class for the HostingpagePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-hostingpage',
  templateUrl: 'hostingpage.html',
})
export class HostingpagePage {
  connecteduser: any;
  user: any = {};

  constructor(public navCtrl: NavController, public navParams: NavParams, public storage: Storage) {
    // this.navCtrl.push(PlayerlistpagePage);
    this.user.name = '';
    this.user.avtar = '';



  }

  ionViewDidLoad() {

    console.log('ionViewDidLoad HostingpagePage');
  }

}
