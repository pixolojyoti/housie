import { Component, Input } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { StatusBar } from '@ionic-native/status-bar';

//PAGES
import { ConnectionoptionsPage } from '../connectionoptions/connectionoptions';

@Component({
	selector: 'page-home',
	templateUrl: 'home.html'
})
export class HomePage {
	avtars = new Array();
	@Input() username: any;
	@Input() useravtar: any;
	static uname = '';
	static uavtar = "";
	constructor(public navCtrl: NavController, private storage: Storage) {
		//this.statusBar.hide();
		
		this.storage.get('playerinfo').then((data) => {
			console.log(data);
			if (data && data.name) {
				this.username = data.name;
				this.useravtar = data.avtar;
				this.navCtrl.push(ConnectionoptionsPage);
			}
		})

		this.avtars = ["avatar_1.png", "avatar_2.png"];

	}
	gotoconnectionsoptions() {
		this.storage.set('playerinfo', { name: this.username, avtar: this.useravtar, coins: 0 });
		console.log(this.storage.get('playerinfo'));
		this.navCtrl.push(ConnectionoptionsPage);
	}




}
