import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { SelectedTicket } from '../selectedticket/selectedticket';
import { NavbarComponent } from '../../components/navbar/navbar';
import { empty } from 'rxjs/Observer';
import { TicketComponent } from '../../components/ticket/ticket';
import { Storage } from '@ionic/storage';
/**
 * Generated class for the HostingpagePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: '',
  templateUrl: 'selectticket.html',
})
export class SelectTicket {
  tickets = new Array();
  ticketnumbers = new Array(9);
  selectedticketindex = 0;
  selectedtickets = new Array();
  constructor(public navCtrl: NavController, public navParams: NavParams, private storage: Storage) {

    console.log(this.ticketnumbers)
    for (var count = 1; count <= 3; count++) {
      this.ticketnumbers = Array.from({ length: 9 }, e => new Array(3).fill(''));
      this.generateticketnumber();
      this.sortnumbers(count - 1);

    };
  }

  ionViewDidLoad() {

    console.log('ionViewDidLoad SelectTicket');
  }

  generateticketnumber() {
    var randomrow = 0, randomcol, randommin = 1, randommax = 9, randomnumber;


    for (var i = 0; i < 15; i++) {
      randomcol = this.ticketnumbers.findIndex((element) => { return element.every((subelement) => subelement == '') > 0 });

      console.log(randomcol);
      if (randomcol == -1) {
        randomnumber = Math.floor((Math.random() * 90) + 1);
        randomcol = Math.floor(randomnumber / 10);
        randomcol = randomcol == 9 ? 8 : randomcol;
      }
      else {

        randommin = randomcol != 0 ? (10 * randomcol) : 1;
        randommax = randomcol != 8 ? (10 * (randomcol + 1)) - 1 : 90;
        randomnumber = Math.floor(Math.random() * (randommax - randommin + 1)) + randommin;

      };
      if (this.ticketnumbers[randomcol].indexOf(randomnumber) == -1) {
        //The Number not exists
        if (this.ticketnumbers[randomcol].includes('')) {
          randomrow = this.getuniquerow(randomcol, [0, 1, 2]);
          if (randomrow == -1) {
            i--;
          }
          else {
            this.ticketnumbers[randomcol][randomrow] = randomnumber;
          }
        }
        else {
          i--;
        }
        // Arrange Numbers in array columns 

      } else if (this.ticketnumbers[randomcol].indexOf(randomnumber) != -1) {
        i--;
      }






    }
    console.log(this.ticketnumbers);
    this.tickets.push(this.ticketnumbers);


  }



  getuniquerow = function (randomcol, rangearray) {
    // debugger; 
    if (rangearray.length != 0) {
      var randomrow = Math.floor((Math.random() * rangearray.length));
      var countperrow = 0;
      for (var i = 0; i < 9; i++) {
        if (this.ticketnumbers[i][rangearray[randomrow]] != '') {
          countperrow += 1;
        }
      }
      if (countperrow < 5) {
        if (this.ticketnumbers[randomcol][rangearray[randomrow]] == '') {
          return rangearray[randomrow];
        }
        else {
          rangearray.splice(randomrow, 1);
          return this.getuniquerow(randomcol, rangearray);

        }

      }
      else {
        rangearray.splice(randomrow, 1);
        return this.getuniquerow(randomcol, rangearray);

      }

    }


    return -1;

  }


  sortnumbers(index) {

    this.tickets[index].forEach((elements) => {
      var smallestnumber;
      for (var outerindex = 0; outerindex < 3; outerindex++) {
        if (elements[outerindex] != '') {
          for (var innerindex = outerindex + 1; innerindex < 3; innerindex++) {
            if (elements[outerindex] > elements[innerindex] && elements[innerindex] != '') {
              var temp = elements[outerindex];
              elements[outerindex] = elements[innerindex];
              elements[innerindex] = temp;
            }
          }

        }

      }
    });
  }



  selectTicket(index) {
    this.selectedticketindex = index + 1;
    this.selectedtickets.push(this.tickets[index]);
    // this.navCtrl.push(SelectedTicket, {
    //   'selectedticket': this.tickets[index]
    // });
  }


  gotoselectedticket() {
    this.storage.remove('tickets');
    this.storage.set('tickets', this.selectedtickets);
    this.navCtrl.push(SelectedTicket);
  }

}
